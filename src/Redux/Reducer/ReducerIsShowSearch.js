const ReducerIsShowSearch = (state = true, action) =>{
    if(action.type === 'IS_SEARCH'){
        return !state;
    }
    return state;
}
export default ReducerIsShowSearch;