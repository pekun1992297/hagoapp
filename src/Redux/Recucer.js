import {combineReducers} from  'redux';

import ReducerIsShowSearch from './Reducer/ReducerIsShowSearch';

const Reducer = combineReducers({
    isShowSearch: ReducerIsShowSearch,
}) ;
export default Reducer;