import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {styleViewGarageContent, styleTouchable, styleText,styleGarageContentTabbar} from '../../StyleCSS/styleView';

export default class ContentGarage extends Component {
    render(){
        const {container, header, content, viewOn, viewLeft, viewRight, viewUnder, viewLeftOn, viewLeftUnder} = styleViewGarageContent;
        const {tabbar, contentTabbar}= styleGarageContentTabbar;
        const {touchableOpacity, touchableTabbar} = styleTouchable;
        const {styleTextTitle , styletext, styletextRating} = styleText;
        return (
            <View style={container}>
                <View style={header}>
                    <View style={viewOn}>

                        <View style={viewLeft}>
                            <View style={viewLeftOn}>
                                <Text style={styleTextTitle}>NHÀ XE PHƯƠNG TRANG</Text>
                            </View>
                            <View style={viewLeftUnder}> 
                                <Text style={styletextRating}>Sao , số lượt đánh giá</Text>
                            </View>
                            
                            

                        </View>

                        <View style={viewRight}>

                            <TouchableOpacity style={touchableOpacity}>
                                <Text style={styletext}>Follow</Text>
                            </TouchableOpacity>
                            <TouchableOpacity  style={touchableOpacity}>
                                <Text style={styletext}>Chỉ đường</Text>
                            </TouchableOpacity>

                        </View>

                    </View>

                    <View style={viewUnder}>
                        <Text style={styletext}>231-233 Lê Hồng Phong, Phường 4, Quận 5, Tp. Hồ Chí Minh</Text>
                    </View>
                </View>
                <View style={content}>
                    <View style={tabbar}>
                        <TouchableOpacity style={touchableTabbar}>
                                <Text style={styletext}>Giới thiệu</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={touchableTabbar}>
                                <Text style={styletext}>Đăng bài</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={touchableTabbar}>
                                <Text style={styletext}>Đánh giá</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={touchableTabbar}>
                                <Text style={styletext}>Liên hệ</Text>
                        </TouchableOpacity>
                    </View>
                    
                    <View style={contentTabbar}>

                    </View>
                </View>
            </View>
        );
    }
}