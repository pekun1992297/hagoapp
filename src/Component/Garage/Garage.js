import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {styleView } from '../../StyleCSS/styleView';
import HeaderGarage from './HeaderGarage';
import ContentGarage from './ContentGarage';


export default class Garage extends Component {
    render(){
        const {container} = styleView;
        return (
            <View style={container}>
                <HeaderGarage/>
                <ContentGarage/>
            </View>
        );
    }
}