import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {styleViewGarageHeader} from '../../StyleCSS/styleView';
import Swiper from 'react-native-swiper';

export default class HeaderGarage extends Component {
    render(){
        const {header, wrapper, slide1, slide2, slide3, text} = styleViewGarageHeader;
        return (
            <View style={header}>
                <Swiper style={wrapper} showsButtons={true}>
                    <View style={slide1}>
                        <Text style={text}>Hello Swiper</Text>
                    </View>
                    <View style={slide2}>
                        <Text style={text}>Beautiful</Text>
                    </View>
                    <View style={slide3}>
                        <Text style={text}>And simple</Text>
                    </View>
                </Swiper>
            </View>
        );
    }
}