import React, {Component} from 'react';
import {View} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import {styleView} from '../../StyleCSS/styleView';
import Header from './Header';
import SearchOne from './Search/SearchOne';

export default class Map extends Component {
 
    render(){
        const {containerMapView} = styleView;
        return (
            <View>
                <MapView
                    style={containerMapView}
                    initialRegion={{
                        latitude: 37.78825,
                        longitude: -122.4324,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                    
                >
                    <Marker 
                        coordinate={{
                            latitude: 37.78825,
                            longitude: -122.4324,
                        }}
                        pinColor='red'
                    />
                </MapView>
                <Header />
                

            </View>
        );
    }
}
   
