import React, {Component} from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import {styleView} from '../../StyleCSS/styleView';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/Entypo';

class Header extends Component {
    constructor(props){
        super(props);
        this.state = {
            search : '',
            // isTabbar: true,

        }

    }

    // componentDidMount() {
    //     this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
    //     this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    // }

    // componentWillUnmount() {
    //     this.keyboardDidShowListener.remove();
    //     this.keyboardDidHideListener.remove();
    // }

    // _keyboardDidShow() {
    //     // alert('Keyboard Shown');
    //     this.setState({ isTabbar: false })
    // }

    // _keyboardDidHide() {
    //     // alert('Keyboard Hidden');
    //     this.setState({ isTabbar: true })
    // }

    render() {
        const { containerHeader, header, headerPositionSearch } = styleView;
        const { search } = this.state;
        return (
            <View style={containerHeader}>
                <View style={headerPositionSearch}>
                    <TouchableOpacity>
                        <Icon name='location' color='#2ecc71' size={30} />
                    </TouchableOpacity>

                </View>

                <View style={header}>
                    <TextInput
                        value={search}
                        onChangeText={(search) => this.setState({ search })}
                        placeholder='Nhập nhà xe cần tìm'
                        underlineColorAndroid='transparent'
                    // onSubmitEditing= {Keyboard.dismiss}
                    />

                </View>

                <View style={headerPositionSearch}>
                    <TouchableOpacity>
                        <Icon name='forward' color='#2ecc71' size={30} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}



export default connect()(Header)