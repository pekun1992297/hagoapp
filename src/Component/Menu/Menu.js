import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {styleView } from '../../StyleCSS/styleView';


export default class Menu extends Component {
    render(){
        const {container} = styleView;
        return (
            <View style={container}>
                <Text>MENU</Text>
            </View>
        );
    }
}