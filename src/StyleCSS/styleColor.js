import { StyleSheet} from 'react-native';


export const styleColor = StyleSheet.create({
    bgcolorCCC: {
        backgroundColor: '#cccccc',
    },
    colorCCC: {
        color: '#cccccc'
    },
    bgcolorC71: {
        backgroundColor: '#2ecc71'
    },
    colorC71:{
        color: '#2ecc71'
    },
    bgcolor828:{
        backgroundColor: '#282828',
    },
    color828:{
        color: '#282828',
    },
    bgcolorWhite:{
        backgroundColor: 'white',
    },
    colorWhite:{
        color:'white',
    },
    colorBlack:{
        color:'black',
    }
})