import {Dimensions, StyleSheet} from 'react-native';

var {width, height} = Dimensions.get('window');
const heighApp = height-(0.04*height)
export const styleView = StyleSheet.create({
    container:{
        flex: 1,
        // justifyContent: 'center',
        // alignItems:'center'
    },
    containerMapView:{
        width: width,
        height: height,
        // position: "absolute",
        // alignSelf: 'stretch',
    },
    containerHeader:{
        // top: 0.02*width,
        // left: 0.02*width,
        margin: 0.02*width,
        marginTop: 0.03*width,
        height: 0.08 * heighApp,
        width: 0.96*width,
        position: "absolute",
        flexDirection: 'row',
        // alignItems:'center',
        backgroundColor: 'white',
    },
    header:{
        backgroundColor: 'white',
        width: 0.96*width-((0.08 * heighApp)*2),
        height: 0.08 * heighApp,
        // alignItems:'center',
        // justifyContent:'center',
    },
    headerPositionSearch:{
        width: 0.08 * heighApp,
        height: 0.08 * heighApp,
        backgroundColor: '#282828',
        alignItems:'center',
        justifyContent:'center',
    }
})

export const styleViewGarageHeader = StyleSheet.create({
    header:{
        flex:3,
        backgroundColor:'gray',
    },
    wrapper: {
    },
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9DD6EB',
    },
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#97CAE5',
    },
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#92BBD9',
    },
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold',
    }
})
export const styleViewGarageContent = StyleSheet.create({
    container:{
        flex:7,
    },
    header:{
        flex: 2,
        backgroundColor:'#282828'
    },
    viewOn:{
        flexDirection:'row',
        margin: 0.02*width,
        // marginBottom:0
    },
        viewLeft:{
            flex:2,
        },
            viewLeftOn:{
                marginBottom: 0.02*width,
            },
            viewLeftUnder:{

            },
        viewRight:{
            flex:1,
            alignItems:'center',
            justifyContent:'space-around'
        },
    viewUnder:{
        marginLeft:0.02*width,
        marginRight:0.02*width,
    },     
    content:{
        flex:7,
        backgroundColor:'white'
    }
})
export const styleGarageContentTabbar = StyleSheet.create({
    tabbar:{
        flexDirection:'row',
        backgroundColor:'pink',
    },
    contentTabbar:{
    },

 })
export const styleTouchable = StyleSheet.create({
   touchableOpacity:{
       width: 0.2 *width,
       height: 0.05*width,
       backgroundColor:'green',
       alignItems: 'center',
       justifyContent:'center',    
   },
   touchableTabbar:{
        width:0.25*width,
        height: 0.08*width,
        backgroundColor:'#282828',
        alignItems:'center',
        justifyContent:'center',
   },
   touchableTabbarInactive:{
    width:0.25*width,
    height: 0.08*width,
    backgroundColor:'white',
   }
})
export const styleText = StyleSheet.create({
    styleTextTitle:{
        fontSize: 0.05*width,
        color: '#2ecc71',
        fontWeight:'bold',
    },
    styletext:{
        color:'white'
    },
    styletextRating:{
        color:'#cccccc'
    }
 })