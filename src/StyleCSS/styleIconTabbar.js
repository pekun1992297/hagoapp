import { Dimensions, StyleSheet } from 'react-native';

var { width } = Dimensions.get('window');
export const styleViewIcon = StyleSheet.create({
    iconView: {
        flex: 1,
        position: 'relative',
        //  alignSelf: 'stretch',
        // alignItems:'center',
        justifyContent: 'center'
    },
    iconImage: {
        width: 0.068 * width,
        height: 0.068 * width,
    },
    badgeView: {
        position: "absolute",
        // top: 0,
        right: 0,
        width: 0.04 * width,
        height: 0.04 * width,
        borderRadius: 50,
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
    },
    badgeText:{
        color:'white',
    }

})