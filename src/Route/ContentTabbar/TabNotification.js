import {createStackNavigator} from 'react-navigation';

import Notification from '../../Component/Notification/Notification';


export const TAB_NOTIFICATION = createStackNavigator({
    NOTIFICATION: {
        screen: Notification,
        navigationOptions:{
            header:null,
        }
    },
})