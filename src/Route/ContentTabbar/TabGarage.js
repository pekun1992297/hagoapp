import {createStackNavigator} from 'react-navigation';

import Garage from '../../Component/Garage/Garage';

export const TAB_GARAGE = createStackNavigator({
    GARAGES: {
        screen: Garage,
        navigationOptions:{
            header:null,
        }
    }
})