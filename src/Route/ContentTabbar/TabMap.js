import {createStackNavigator} from 'react-navigation';

import Map from '../../Component/Map/Map';
import Garage from '../../Component/Garage/Garage';

export const TAB_MAP = createStackNavigator({
    MAP: {
        screen: Map,
        navigationOptions:{
            header:null,
        }
    },
    GARAGES: {
        screen: Garage,
        navigationOptions:{
            header:null,
        }
    }
})