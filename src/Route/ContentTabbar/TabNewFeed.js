import {createStackNavigator} from 'react-navigation';

import NewFeed from '../../Component/NewFeed/NewFeed';


export const TAB_NEWFEED = createStackNavigator({
    NEWFEED: {
        screen: NewFeed,
        navigationOptions:{
            header:null,
        }
    },
})