import {createStackNavigator} from 'react-navigation';

import Menu from '../../Component/Menu/Menu';


export const TAB_MENU = createStackNavigator({
    MENU: {
        screen: Menu,
        navigationOptions:{
            header:null,
        }
    },
})