import React, {Component} from 'react';
import {View, Image, Text} from 'react-native';

import {styleViewIcon} from '../../StyleCSS/styleIconTabbar';

import icuser1 from '../../Image/icon/tabbar/ic-user.png';
import icuser0 from '../../Image/icon/tabbar/ic-user0.png';

export default class IconGarage extends Component{
    constructor(props){
        super(props);
        this.state={
            noti: []
        }
    }
    render(){
        const { iconView, iconImage, badgeText, badgeView } = styleViewIcon;
        const { focused } = this.props;
        return (
            <View style={iconView}>
                {
                    focused ? <Image source={icuser1} style={iconImage} /> : <Image source={icuser0} style={iconImage} />
                }

                {
                    this.state.noti.length > 0 ?
                        < View style={badgeView}>
                            <Text style={badgeText}>{this.state.noti.length}</Text>
                        </View>
                        : undefined
                }

            </View>
        );
    }
}