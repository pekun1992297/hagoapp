import React, {Component} from 'react';
import {View, Image, Text} from 'react-native';

import {styleViewIcon} from '../../StyleCSS/styleIconTabbar';

import icmap1 from '../../Image/icon/tabbar/ic-map.png';
import icmap0 from '../../Image/icon/tabbar/ic-map0.png';

export default class IconMap extends Component{
    constructor(props){
        super(props);
        this.state={
            noti: []
        }
    }
    render(){
        const { iconView, iconImage, badgeText, badgeView } = styleViewIcon;
        const { focused } = this.props;
        return (
            <View style={iconView}>
                {
                    focused ? <Image source={icmap1} style={iconImage} /> : <Image source={icmap0} style={iconImage} />
                }

                {
                    this.state.noti.length > 0 ?
                        < View style={badgeView}>
                            <Text style={badgeText}>{this.state.noti.length}</Text>
                        </View>
                        : undefined
                }

            </View>
        );
    }
}