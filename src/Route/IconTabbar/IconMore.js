import React, {Component} from 'react';
import {View, Image, Text} from 'react-native';

import {styleViewIcon} from '../../StyleCSS/styleIconTabbar';

import icmore1 from '../../Image/icon/tabbar/ic-more.png';
import icmore0 from '../../Image/icon/tabbar/ic-more0.png';

export default class IconMore extends Component{
    constructor(props){
        super(props);
        this.state={
            noti: []
        }
    }
    render(){
        const { iconView, iconImage, badgeText, badgeView } = styleViewIcon;
        const { focused } = this.props;
        return (
            <View style={iconView}>
                {
                    focused ? <Image source={icmore1} style={iconImage} /> : <Image source={icmore0} style={iconImage} />
                }

                {
                    this.state.noti.length > 0 ?
                        < View style={badgeView}>
                            <Text style={badgeText}>{this.state.noti.length}</Text>
                        </View>
                        : undefined
                }

            </View>
        );
    }
}