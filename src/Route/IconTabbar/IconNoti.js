import React, {Component} from 'react';
import {View, Image, Text} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

import {styleViewIcon} from '../../StyleCSS/styleIconTabbar';

// import icnoti1 from '../../Image/icon/tabbar/ic-notification.png';
// import icnoti0 from '../../Image/icon/tabbar/ic-notification0.png';

export default class IconNoti extends Component{
    constructor(props){
        super(props);
        this.state={
            noti: [{id:1},{id:2},{id:3}]
        }
    }
    render(){
        const { iconView, iconImage, badgeText, badgeView } = styleViewIcon;
        const { tintColor } = this.props;
        return (
            <View style={iconView}>
                {/* {
                    focused ? <Image source={icnoti1} style={iconImage} /> : <Image source={icnoti0} style={iconImage} />
                } */}
                <Icon name='bell' color={tintColor} size={24}/>
                {
                    this.state.noti.length > 0 ?
                        < View style={badgeView}>
                            <Text style={badgeText}>{this.state.noti.length}</Text>
                        </View>
                        : undefined
                }

            </View>
        );
    }
}