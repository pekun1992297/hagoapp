import React from 'react';
import {createBottomTabNavigator} from 'react-navigation';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/Entypo';
import {connect} from 'react-redux';

import {TAB_MAP} from './ContentTabbar/TabMap';
import {TAB_GARAGE} from './ContentTabbar/TabGarage';
import {TAB_NEWFEED} from './ContentTabbar/TabNewFeed';
import {TAB_NOTIFICATION} from './ContentTabbar/TabNotification';
import {TAB_MENU} from './ContentTabbar/TabMenu';

import IconMap from './IconTabbar/IconMap';
import IconGarage from './IconTabbar/IconGarage';
import IconNews from './IconTabbar/IconNews';
import IconNoti from './IconTabbar/IconNoti';
import IconMore from './IconTabbar/IconMore';

// const {isTabbar} = this.props;
const TABBAR = createBottomTabNavigator({
    
    mapTab: {
        screen: TAB_MAP,
        navigationOptions:{
            // tabBarVisible:true,
            tabBarLabel:'Map',
            // tabBarIcon:({focused})=> (<IconMap focused={focused}/>)
            tabBarIcon: ({tintColor})=>( <Icon name='globe' color={tintColor} size={24}/>),
        },
    },
    garageTab:{
        screen: TAB_GARAGE,
        navigationOptions:{
            tabBarLabel:'Nhà xe',
            // tabBarIcon:({focused})=>(<IconGarage focused={focused}/>)
            tabBarIcon: ({tintColor})=>( <Icon name='user' color={tintColor} size={24}/>)
        },
    },
    newfeedTab:{
        screen: TAB_NEWFEED,
        navigationOptions:{
            tabBarLabel:'Bản tin',
            // tabBarIcon:({focused})=>(<IconNews focused={focused}/>)
            tabBarIcon: ({tintColor})=>( <Icon name='news' color={tintColor} size={24}/>)
        },
    },
    notificationTab:{
        screen: TAB_NOTIFICATION,
        navigationOptions:{
            tabBarLabel:'Thông báo',
            tabBarIcon:({tintColor})=>(<IconNoti tintColor={tintColor}/>)
            // tabBarIcon: ({tintColor})=>( <Icon name='bell' color={tintColor} size={24}/>)
        },
    },
    menutab:{
        screen: TAB_MENU,
        navigationOptions:{
            tabBarLabel:'Xem thêm',
            // tabBarIcon:({focused})=>(<IconMore focused={focused}/>)
            tabBarIcon: ({tintColor})=>( <Icon name='dots-three-horizontal' color={tintColor} size={24}/>)
        },
    }
},{
    initialRouteName: 'mapTab',
    tabBarOptions:{
        activeTintColor: '#2ecc71',
        style: {
            backgroundColor:'#282828',
            
        }
    },
    
    // order:['newfeedTab','garageTab','mapTab','notificationTab','menutab'],
    
    // inactiveTintColor :'#cccccc',
    // activeTintColor: '#2ecc71',
    // barStyle:{
    //     backgroundColor:'#282828',
    // },
    
})

export function mapStoreToProps(state){
    return{
        isTabbar: state.isTabbar,
    }
}

export default connect(mapStoreToProps)(TABBAR);