
import React, { Component } from 'react';
import Main from './src/Component/Main';
import {Provider} from 'react-redux';
import store from './src/Redux/Store';


export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Main/>
      </Provider>
    );
  }
}

